package ordersystem;
/*
	Copyright (C) 2018 Karl R. Wurst
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * Controller for REST API endpoints for Customers
 * 
 * @author Karl R. Wurst
 * @version Fall 2018
 */
@RestController
public class CustomerController {

	private static final AtomicLong counter = Database.getCustomerCounter();
    private static Map<Long, Customer> customerDb = Database.getCustomerDb();
  
    /**
     * Create a new customer in the database
     * @param customer customer with first and last names
     * @return the customer number
     */
    @CrossOrigin() // to allow CORS requests when running as a local server
    @PostMapping("/customers/new")
    public ResponseEntity<Long> addNewCustomer(@RequestBody Customer customer) {
    	customer.setNumber(counter.incrementAndGet());
    	customerDb.put(customer.getNumber(), customer);
    	return new ResponseEntity<>(customer.getNumber(), HttpStatus.CREATED);
    }
    
    /**
     * Get a customer from the database
     * @param number the customer number
     * @return the customer if in the database, not found if not
     */
    @GetMapping("/customers/{number}")
    public ResponseEntity<Object> getCustomerByNumber(@PathVariable long number) {
    	if (customerDb.containsKey(number)) {
            return new ResponseEntity<>(customerDb.get(number), HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>("Customer does not exist", HttpStatus.NOT_FOUND);
    	}
    }
    @GetMapping("/customers/getAll")
    public ResponseEntity<Object> getAllCustomer() {
    	if (customerDb.isEmpty()==true) {
            return new ResponseEntity<>("No Customers In Database",HttpStatus.NOT_FOUND);
            } 
    	else {
    		return new ResponseEntity<>(customerDb.values(),HttpStatus.OK);
    		}
    	}
    @CrossOrigin()
    @PutMapping("/customers/{customerNumber}/updateName/{firstName}/{lastName}")
    public ResponseEntity<String> setCustomerName(@PathVariable long customerNumber,@PathVariable String firstName,@PathVariable String lastName) {
    	if (customerDb.containsKey(customerNumber)==false) {
            return new ResponseEntity<>("Customer Not Found In Database",HttpStatus.NOT_FOUND);
            } 
    	else {
    		customerDb.get(customerNumber).setFirstName(firstName);
    		customerDb.get(customerNumber).setLastName(lastName);
    		return new ResponseEntity<>("Customer Name Updated",HttpStatus.OK);
    		}
    	}
    @GetMapping("/customers/getNumberByName/{firstName}/{lastName}")
    public ResponseEntity<Object> getCustomerByName(@PathVariable String firstName,@PathVariable String lastName) {
    	ArrayList<Long> id = new ArrayList<Long>();  
    	customerDb.forEach((k,v)->{
    		if(v.getFirstName().equals(firstName)==true && v.getLastName().equals(lastName)==true){
    			id.add(v.getNumber());
    			}
    		}
    	);
    	if(id.isEmpty()==true) {
    		return new ResponseEntity<>("Customer Not Found In Database",HttpStatus.NOT_FOUND);
    		}
    	else {
    		return new ResponseEntity<>(id,HttpStatus.OK);
    		}
    	}
    }