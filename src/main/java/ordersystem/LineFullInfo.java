package ordersystem;
public class LineFullInfo {
	private long sku;
	private String description;
	private double unitprice;
	private int quantity;
	private double cost;
	public LineFullInfo(long sku,String description,double unitprice,int quantity) {
		this.sku=sku;
		this.description=description;
		this.unitprice=unitprice;
		this.quantity=quantity;
		this.cost=unitprice*quantity;
		}
	public long getSku() {
		return sku;
	}
	public String getDescription() {
		return description;
	}
	public double getUnitprice() {
		return unitprice;
	}
	public int getQuantity() {
		return quantity;
	}
	public double getCost() {
		return cost;
	}
	public void setSku(long sku) {
		this.sku=sku;
	}
	public void setDescription(String description) {
		this.description=description;
	}
	public void setUnitprice(double unitprice) {
		this.unitprice=unitprice;
	}
	public void setQuantity(int quantity) {
		this.quantity=quantity;
	}
	public void setCost(double cost) {
		this.cost=cost;
	}
}
