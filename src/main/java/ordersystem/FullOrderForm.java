package ordersystem;

import java.util.ArrayList;

public class FullOrderForm {
	private Customer customer;
	ArrayList<LineFullInfo> lines;
	private double totalCost;
	public FullOrderForm(Customer customer) {
		this.customer=customer;
		lines = new ArrayList<LineFullInfo>();
		totalCost=0;
	}
	public void addToFullOrderForm(LineFullInfo line) {
		lines.add(line);
		totalCost=totalCost+line.getCost();
	}
	public Customer getCustomer() {
		return customer;
	}
	public ArrayList<LineFullInfo> getLines() {
		return lines;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setCustomer(Customer customer) {
		this.customer=customer;
	}
	public void setLines(ArrayList<LineFullInfo> lines) {
		this.lines=lines;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost=totalCost;
	}
}
